var bookingCarId;
var module = {
	lists:{}, 
	assignModel:{},
	datas : [],
	queryData : function() {
		var param=$('#queryForm').serialize();
		ajx.getData({
			url : './returningCar/find?'+param,
			callback : function(models) {
				if(models[0]["returnTime"]==null){
					$('#bookingDate').html(models[0]["bookingDate"]);
					$('#projId').html(models[0]["projId"]);
					$('#startTime').html(models[0]["startTime"]);
					$('#endTime').html(models[0]["endTime"]);
					$('#cost').html(models[0]["cost"]);
					$('#destination').html(models[0]["destination"]);
					$('section[role="update"]').show(200);
					bookingCarId=models[0]["bookingCarId"];
				}
			}
		});
		
	},
	updateData : function() {
		if(!module.formvalEdit())
			return;
		var as=$('#updateForm').serializeArray();
		var pa={};
		for(var i in as){
			pa[as[i].name]=as[i].value;
		}
		pa["bookingCarId"]=bookingCarId;
//		console.debug(pa);
		ajx.postData({
			url : './project/update',
			data : pa,
			callback : function(datas) {
				console.debug(datas);
				if (datas == 'error') {
					swal('更新失敗');
				} else {
					module.queryData();
					ui.gotoMode('MODE_UPDATE_CANCEL');
				}
			}
		});
	},
	formvalEdit:function(){
		return true;
	},
	formvalCreate:function(){
		return true;
	}
};

var ui = {
	showConfirmDialog:function(action,p1,p2){
		var confirmText,backFn;
		if(action=='CONFIRM_UPDATE'){
			if(!module.formvalEdit()){
				return;
			}
			confirmText="確定要送出嗎?";
			backFn=module.updateData;
		}
		swal({
			  title: confirmText,
			  type: "warning",
			  showCancelButton: true,
			  confirmButtonColor: "red",
			  confirmButtonText: "確定",
			  cancelButtonText: "取消",
			  closeOnConfirm: true,
			  closeOnCancel: true
			},
			function(isConfirm){
			  if (isConfirm&&backFn!=undefined) {
				  backFn();
			  }
			}
		);		
	}
};

$(document).ready(function() {
	$("#updateForm").on("submit",function( event ) {
		event.preventDefault();
	});
	module.queryData();
});
