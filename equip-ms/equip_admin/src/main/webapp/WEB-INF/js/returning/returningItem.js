var module = {
	lists:{}, 
	assignModel:{},
	datas : [],
	queryData : function() {
		var param=$('#queryForm').serialize();
		ajx.getData({
			url : './returningItem/find_return?'+param,
			callback : function(models) {
				module.datas=models;
				var source=$('#detail-template').html();
				var template= Handlebars.compile(source);
				var html= template({ds:models});
				$('#tableData > tbody').html('').html(html);
				console.debug("queryData:");
				console.debug(module.datas);
			}
		});
	},
	queryPage:function(){
		var param=$('#queryForm').serialize();
		ajx.getData({
			url : './returningItem/findPage_return?'+param,
			callback : function(d) {
				console.debug(d);
				var html='';
				var page=d.total/d.page_num;
				if(d.total%d.page_num>0){
					page+=1;
				}
				for(var i=1;i<=page;i++){
					html+='<option value="'+i+'">第'+i+'頁</option>';
				}
				$('#qpage').html('').html(html);
				$('#totalRecord').html(d.total);
			}
		});			
	},
	updateData :function() {
		if(!module.formvalEdit())
			return;
		var as = module.assign;
		ajx.postData({
			url : './returningItem/update',
			data : as,
			callback : function(datas) {
				console.debug(datas);
				console.debug(as);
				if (datas == 'error') {
					swal('歸還失敗');
				} else {
					module.queryData();
					ui.gotoMode('MODE_UPDATE_CANCEL');
				}
			}
		});
	},
	formvalEdit:function(){
		return true;
	},
	formvalCreate:function(){
		return true;
	}
};
var textInputs;
var ui = {
		gotoMode:function(action,p1,p2){
			if(action=='MODE_UPDATE_CANCEL'){
				$('section[role="update"]').hide();
				$('section[role="query"]').show(200);
			}
		},
		showConfirmDialog:function(action,p1,p2){
			var confirmText,backFn;
			if(action=='CONFIRM_UPDATE'){
				for(var i in module.datas)
				{
				 if(p1==module.datas[i].bookingItemId){
					 module.assign=module.datas[i];
					 break;
				 }
				}
				
				if(!module.formvalEdit()){
					return;
				}
				confirmText="確定歸還?";
				backFn=module.updateData;
			}
			
			swal({
				  title: confirmText,
				  type: "warning",
				  showCancelButton: true,
				  confirmButtonColor: "red",
				  confirmButtonText: "確認",
				  cancelButtonText: "取消",
				  closeOnConfirm: true,
				  closeOnCancel: true
				},
				function(isConfirm){
				  if (isConfirm&&backFn!=undefined) {
					  backFn();
				  }
				}
			);		
		}
			
};

$(document).ready(function(){

	$( "#queryForm" ).on( "submit", function( event ) {
		  event.preventDefault();
		  $('#queryForm input[name="page"]').val('1');
		  module.queryData();
		  module.queryPage();
	});
	
	$( "#updateForm" ).on( "submit", function( event ) {
		event.preventDefault();
	});

	$( "#qpage" ).change( function( event ) {
		$('#queryForm input[name="page"]').val($(this).val());
		 module.queryData();
		 module.queryPage();
	});
	
	module.queryData();
	module.queryPage();
	
});
