var module = {
	lists:{}, 
	assignModel:{},
	datas : [],
	queryData : function(type) {
		var param=$('#queryForm').serialize();
		ajx.getData({
			url : ui.url[type]+"?"+param,
			callback : function(models) {
				console.debug(url);
				module.datas=models;
				
				if(type=="item_bookReturn"){
					for(var i=0;i<models.length;i++){
						if(models[i]["returnTime"]!=null){
							models[i]["qty"]="+"+models[i]["qty"];
						}else{
							models[i]["qty"]="-"+models[i]["qty"];
						}
					}
				}
				
				var source=$("#"+type).html();
				var template= Handlebars.compile(source);
				var html= template({ds:models});
				$('#tableData > tbody').html('').html(html);
				
				var page=models.length/16;
				if(models.length%16>0){
					page+=1;
				}
				html="";
				for(var i=1;i<=page;i++){
					html+='<option value="'+i+'">第'+i+'頁</option>';
				}
				$('#qpage').html('').html(html);
				$('#totalRecord').html(models.length);
			}
		});
	},
	formvalEdit:function(){
		return true;
	},
	formvalCreate:function(){
		return true;
	}
};

var ui = {
		tableColumn:[],
		url:[],
		page:[],
		changeTableHeader:function(type){
			var source=$('#th-template').html();
			var template= Handlebars.compile(source);
			var context=ui.tableColumn[type];
//			console.debug("context="+context);
			var html=template({th:context});
			$('#tableData > thead').html('').html(html);
		},
		getTableRow:function(){
			ui.tableColumn["car_proj"]=[{thContext:"計畫代號"},{thContext:"借車次數"}];
			ui.tableColumn["car_depart"]=[{thContext:"部門名稱"},{thContext:"借車次數"}];
			ui.tableColumn["car_user"]=[{thContext:"姓名"},{thContext:"借車次數"}];
			ui.tableColumn["car_notReturn"]=[{thContext:"姓名"},{thContext:"未填還車單次數"}];
			ui.tableColumn["item_stock"]=[{thContext:"資材名稱"},{thContext:"庫存數量"}];
			ui.tableColumn["item_bookReturn"]=[{thContext:"交易時間"},{thContext:"資材名稱"},{thContext:"借出/歸還數量"}];
			ui.url["car_proj"]="./report/findCarProj";
			ui.url["car_depart"]="./report/findCarDepart";
			ui.url["car_user"]="./report/findCarUser";
			ui.url["car_notReturn"]="./report/findCarNotReturn";
			ui.url["item_stock"]="./report/findItemStock";
			ui.url["item_bookReturn"]="./report/findItemBookReturn";
		},
		changeTime:function(v){//開始時間變動後
			console.log("v="+v);
		    $("#endTime").attr({"min":v});

		},
		changeItemType:function(v){
			if(v=="item_bookReturn"){
				$("#item_type").show(200);
			}else{
				$("#item_type").hide();
			}
		}
};

$(document).ready(function(){
	
	$( "#startTime" ).on("change",function(){
		ui.changeTime($(this).val());
	});
	
	$( "#type" ).on("change",function(){
		console.debug("type："+$(this).val());
		ui.changeItemType($(this).val());
	});
	
	$( "#queryForm" ).on( "submit", function( event ) {
		  event.preventDefault();
		  $('#queryForm input[name="page"]').val('1');
		  var type=$('#queryForm select[name="type"]').val();
		  ui.changeTableHeader(type);
		  module.queryData(type);
	});
	
	$( "#updateForm,#createForm" ).on( "submit", function( event ) {
		event.preventDefault();
	});

	$( "#qpage" ).change( function( event ) {
		$('#queryForm input[name="page"]').val($(this).val());
		 module.queryData(type);
	});
	
	ui.getTableRow();
	ui.changeTableHeader("car_proj");
	module.queryData("car_proj");

});
