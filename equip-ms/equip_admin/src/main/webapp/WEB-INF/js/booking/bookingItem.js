var module = {
	lists:{}, 
	assignModel:{},
	datas : [],
	queryItemId : function() {
		var p={};
		var param=$('#queryForm').serialize();
		ajx.getData({
			url : './bookingItem/find?'+param,
			callback : function(models) {
				module.datas=models;
				var source=$('#detail-template').html();
				var template= Handlebars.compile(source);
				var html= template({ds:models});
				$('#tableData > tbody').html('').html(html);
				module.datas=models;
				console.log(models);
				$("#itemId").empty();
				for(var i=0;i<models.length;i++){
					//p[models[i]["itemId"]]=models[i]["quantity"];
					$("#itemId").append($('<option>',{
					    value: models[i]["itemId"],
					    text: models[i]["itemName"]
					}));
				}
			}
		});
	},
	queryProjId : function() {
		ajx.getData({
			url : './project/find',
			callback : function(models) {
			    module.datas=models;
				console.log(models);
				$("#projId").empty();
				for(var i=0;i<models.length;i++){
					$("#projId").append($('<option>',{
					    value: models[i]["projId"],
					    text: models[i]["projId"]
					}));
				}
			}
		});
	},
	queryPage:function(){
		var param=$('#queryForm').serialize();
		ajx.getData({
			url : './bookingItem/findPage?'+param,
			callback : function(d) {
				console.debug(d);
				var html='';
				var page=d.total/d.page_num;
				if(d.total%d.page_num>0){
					page+=1;
				}
				for(var i=1;i<=page;i++){
					html+='<option value="'+i+'">第'+i+'頁</option>';
				}
				$('#qpage').html('').html(html);
				$('#totalRecord').html(d.total);
			}
		});			
	},
	createData :function() {
		if(!module.formvalCreate())
			return;
		var as=$('#createForm').serializeArray();
		var pa={};
		for(var i in as){
			pa[as[i].name]=as[i].value;
		}
		ajx.postData({
			url : './bookingItem/create',
			data : pa,
			callback : function(datas) {
				console.debug(datas);
				if (datas == 'error') {
					swal('預約失敗');
				} else {
					ui.showSuccessDialog();
				}
			}
		});
	},
	
	updateData :function() {
		if(!module.formvalCreate())
			return;
		var as=$('#createForm').serializeArray();
		var pa={};
		for(var i in as){
			pa[as[i].name]=as[i].value;
		}
		ajx.postData({
			url : './bookingItem/update',
			data : pa,
			callback : function(datas) {
				console.debug(datas);
				if (datas == 'error') {
					swal('修改失敗');
				} else {
					//ui.showSuccessDialog();
					ui.gotoMode('MODE_CREATE_CANCEL');
				}
			}
		});
	},
	
	formvalEdit:function(){
		return true;
	},
	formvalCreate:function(){
		return true;
	}
};

var ui = {
		gotoMode:function(action,p1,p2){
			if(action=='MODE_CREATE'){
				$('section[role="create"] input').val('');	
				$('section[role="query"]').hide();
				$('section[role="create"]').show(200);
			}else if(action=='MODE_CREATE_CANCEL'){
				$('section[role="create"]').hide();
				$('section[role="create"] input').val('');
				$('#projId').val($('#projId option:first').val());
				$('#startTime').val(0);
				ui.changeTime(0);
				module.queryItemId();
				$('section[role="query"]').show(200);
			}
		},
		showConfirmDialog:function(action,p1,p2){
			var confirmText,backFn;
			if(action=='CONFIRM_CREATE'){
				if(!module.formvalCreate()){
					return;
				}
				confirmText="確定要預約嗎?";
				backFn=module.createData;
			}
			swal({
				  title: confirmText,
				  type: "warning",
				  showCancelButton: true,
				  confirmButtonColor: "red",
				  confirmButtonText: "確定",
				  cancelButtonText: "取消",
				  closeOnConfirm: true,
				  closeOnCancel: true
				},
				function(isConfirm){
				  if (isConfirm&&backFn!=undefined) {
					  backFn();
				  }
				}
			);		
		},
		showSuccessDialog:function(){
			swal({
				  title: "預約成功",
				  showCancelButton: false,
				  confirmButtonColor: "red",
				  confirmButtonText: "確定",
				  closeOnConfirm: true
			},function(isConfirm){
				module.updateData();
				//ui.gotoMode('MODE_CREATE_CANCEL');
			});
		},
		changeTime:function(v){//開始時間變動後
			$("#endTime").empty();
			var stime=1+parseInt(v)
			for (var i=stime ;i<=23; i++) {
				$("#endTime").append($('<option>',{
				    value: i,
				    text: i
				}));
			}
		}
};

$(document).ready(function() {

	$( "#queryForm" ).on( "submit", function( event ) {
		  event.preventDefault();
		  $('#queryForm input[name="page"]').val('1');
		  module.queryItemId();
		  module.queryPage();
	});
	
	$( "#startTime" ).on("change",function(){
		ui.changeTime($(this).val());
	});
	
	$( "#createForm" ).on( "submit", function( event ) {
		event.preventDefault();
	});

	$( "#qpage" ).change( function( event ) {
		$('#queryForm input[name="page"]').val($(this).val());
		 module.queryItemId();
	});
	
	ui.changeTime(0);
	module.queryItemId();
	module.queryProjId();
	module.queryPage();
	
});
