var module = {
	queryModel:{},
	events:[],
	lists:{}, 
	assignModel:{},
	datas : [],
	queryProjId : function() {
		ajx.getData({
			url : './bookingCar/findCanUseProj',
			callback : function(models) {
			    module.datas=models;
				//console.log(models);
				$("#projId").empty();
				for(var i=0;i<models.length;i++){
					$("#projId").append($('<option>',{
					    value: models[i]["projId"],
					    text: models[i]["projId"]
					}));
				}
			}
		});
	},
	queryData:function() {
		ajx.getData({
			url : './bookingCar/find',
			callback : function(models) {
				console.log(models);
				module.events.length=0;
				for(var i=0;i<models.length;i++){
					var bookingDate=models[i]["bookingDate"];
					var sTime=models[i]["startTime"];
					var eTime=models[i]["endTime"];
					module.events[i]=module.getEvent(bookingDate,sTime,eTime);
				}
				module.queryModel=models;
				ui.initCalendar();
			}
		});
	},
	createData :function(){
		if(!module.formvalCreate())
			return;
		var as=$('#createForm').serializeArray();
		var pa={};
		for(var i in as){
			pa[as[i].name]=as[i].value;
		}
//		console.debug(pa);
//		return;
		ajx.postData({
			url : './bookingCar/create',
			data : pa,
			callback : function(datas) {
				console.debug(datas);
				if (datas == 'error') {
					swal('預約失敗');
				} else {
					ui.showSuccessDialog();
				}
				var obj=module.getEvent(pa["bookingDate"],pa["startTime"],pa["endTime"]);
				$('#calendar').fullCalendar('renderEvent', obj ,true);
				console.log(obj);
			}
		});
	},
	getEvent:function(day,sTime,eTime){
		var date=day.split("-");
		var obj={
				title:'',
				start: new Date(date[0],date[1]-1, date[2], sTime, 0),
			    end:new Date(date[0],date[1]-1, date[2], eTime, 0)
		};
		return obj;
	},
	formvalEdit:function(){
		return true;
	},
	formvalCreate:function(){
		return true;
	}
};

var ui = {
		CantBookTime:[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
		gotoMode:function(action,p1,p2){
			if(action=='MODE_CREATE_CANCEL'){
				$('section[role="create"] input').val('');
				$('#projId').val($('#projId option:first').val());
				$('#startTime').val(0);
				$('section[role="calendar"]').show(200);
				$('section[role="create"]').hide();
			}else if(action=='MODE_CREATE'){
				ui.changeStartTime();
	            $('section[role="calendar"]').hide();
				$('section[role="create"]').show();
			}
		},
		showConfirmDialog:function(action,p1,p2){
			var confirmText,backFn;
			if(action=='CONFIRM_CREATE'){
				if(!module.formvalCreate()){
					return;
				}
				confirmText="確定要預約嗎?";
				backFn=module.createData;
			}
			swal({
				  title: confirmText,
				  type: "warning",
				  showCancelButton: true,
				  confirmButtonColor: "red",
				  confirmButtonText: "確定",
				  cancelButtonText: "取消",
				  closeOnConfirm: true,
				  closeOnCancel: true
				},
				function(isConfirm){
				  if (isConfirm&&backFn!=undefined) {
					  backFn();
				  }
				}
			);		
		},
		showSuccessDialog:function(){
			swal({
				  title: "預約成功",
				  showCancelButton: false,
				  confirmButtonColor: "red",
				  confirmButtonText: "確定",
				  closeOnConfirm: true
			},function(isConfirm){
				ui.gotoMode('MODE_CREATE_CANCEL');
			});
		},
		changeEndTime:function(v){//開始時間變動後
			console.log("in changeEndTime");
			$("#endTime").empty();
			var stime=parseInt(v);
			var i=stime;
			while((i<=23)&&ui.CantBookTime[i]!=1){
				$("#endTime").append($('<option>',{
				    value: i,
				    text: i
				}));
				i++;
			}
		},
		changeStartTime:function(){
			ui.CantBookTime=[];
			console.log("in changeStartTime");
			var useTime=[];
			$("#startTime").empty();
			var day=$('#createForm input[name="bookingDate"]').val();//點擊到哪天
			for(var i=0;i<module.queryModel.length;i++){
				if(module.queryModel[i]["bookingDate"]== day){
					useTime.push(module.queryModel[i]["startTime"]);
					useTime.push(module.queryModel[i]["endTime"]);
				}
			}
			//console.log(useTime);
			//console.log("useTlength/2="+useTime.length/2)
			for(var i=0;i<useTime.length;i+=2){
				for(var j=useTime[i];j<=useTime[i+1];j++){
					ui.CantBookTime[j]=1;
				}
			}
			var first=null;
			//console.log(ui.CantBookTime);
			for(var i=0;i<23;i++){
				if(ui.CantBookTime[i]!=1){
					$("#startTime").append($('<option>',{
					    value: i,
					    text: i
					}));
					if(first==null){
						first=i;
					}
				}
			}
			ui.changeEndTime(first);
		},
		initCalendar:function(){//日曆初始化
			console.log("initCalendar start");
			var $calendar = $('#calendar');
			//console.log(module.events);
			$calendar.fullCalendar({
				header: {
					left: 'title',
					right: 'prev,next'
				},
				themeButtonIcons: {
					prev: 'fa fa-caret-left',
					next: 'fa fa-caret-right',
				},
				editable:true,
				timeFormat: 'H:mm',
				displayEventEnd:true,
		        dayClick: function(date, allDay, jsEvent, view) { 
		        	var today = new Date();
		        	//console.log(today);
		        	if(date-today>0||today-date<86400000){
			        	var chooseDay=date.format();
						$('#createForm input[name="bookingDate"]').val(chooseDay);
						ui.gotoMode("MODE_CREATE");
					}
		        },
				events:module.events
			});
			//根據bootstrap版本做更動
			var $calendarButtons = $calendar.find('.fc-header-right > span');
			$calendarButtons
				.filter('.fc-button-prev, .fc-button-today, .fc-button-next')
					.wrapAll('<div class="btn-group mt-sm mr-md mb-sm ml-sm"></div>')
					.parent()
					.after('<br class="hidden"/>');

			$calendarButtons
				.not('.fc-button-prev, .fc-button-today, .fc-button-next')
					.wrapAll('<div class="btn-group mb-sm mt-sm"></div>');

			$calendarButtons
				.attr({ 'class': 'btn btn-sm btn-default' });
		}
};
$(document).ready(function() {
	
	$( "#startTime" ).on("change",function(){
		ui.changeEndTime($(this).val());
	});

	$( "#createForm" ).on( "submit", function( event ) {
		event.preventDefault();
	});	
	module.queryData();
	module.queryProjId();

});
