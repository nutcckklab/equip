<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!doctype html>
 <!-- sidebar-light -->
<html class="fixed sidebar-light" lang="zh-Hant-TW">
<%@include file="../include/includeHeader.jsp"%>
	<body>
		<section class="body">

			<!-- start: header -->
<%@include file="../include/includeTopHeader.jsp"%>
			<!-- end: header -->

			<div class="inner-wrapper">
				<!-- start: sidebar -->
<%@include file="../include/includeMenu.jsp"%>
				<!-- end: sidebar -->

				<section role="main" class="content-body">
					<header class="page-header">
						<h2>首頁</h2>
					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="index.html">
										<i class="fa fa-home"></i>
									</a>
								</li>
								<li><span>首頁</span></li>
							</ol>
							&nbsp;&nbsp;&nbsp;
						</div>
					</header>
					
					<!-- 查詢 area -->
						<section class="panel panel-featured-primary panel-featured" role="query">
							<header class="panel-heading">
							<h3 class="panel-title"><i class="el el-search">借車概況</i></h3>
							</header>						
							<div class="panel-body">
								<div class="row">
									<form id="queryForm">
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">上一次歸還時間：</label>
												<span  id=returnTime></span>
											</div>
											<div class="form-group">
												<label class="control-label">結束里程數：</label>
												<span  id=endMilage></span>
											</div>
											<div class="form-group">
												<label class="control-label">放置地點：</label>
												<span id=carLocation></span>
											</div>
										</div>
									</form>
								</div>							
							</div>
						</section>
</section>
<%@include file="../include/includeFooterJs.jsp"%>
		<script src="js/index/index.js?<c:out value="${applicationScope.js_version}"/>"></script>
	</body>
</html>