<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!doctype html>
 <!-- sidebar-light -->
<html class="fixed sidebar-light" lang="zh-Hant-TW">
<%@include file="../include/includeHeader.jsp"%>
	<body>
		<section class="body">

			<!-- start: header -->
<%@include file="../include/includeTopHeader.jsp"%>
			<!-- end: header -->

			<div class="inner-wrapper">
				<!-- start: sidebar -->
<%@include file="../include/includeMenu.jsp"%>
				<!-- end: sidebar -->

				<section role="main" class="content-body">
					<header class="page-header">
						<h2>歸還</h2>
					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="index.html">
										<i class="fa fa-home"></i>
									</a>
								</li>
								<li><span>歸還</span></li>
								<li><span>資材歸還</span></li>
							</ol>
							&nbsp;&nbsp;&nbsp;
						</div>
					</header>

					<!-- start: page -->
																							
						<!-- 查詢 area -->
						<section class="panel panel-featured-primary panel-featured" role="query">
							<header class="panel-heading">
							<h3 class="panel-title"><i class="el el-search">查詢條件</i></h3>
							</header>						
							<div class="panel-body">
								<div class="row">
									<form id="queryForm">
										<input name="page" type="hidden">
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">計畫代號</label>
												<input name="projId" class="form-control">
											</div>
										</div>
										<div class="col-md-2">
											<div class="form-group">
												<label class="control-label">&nbsp;</label>
												<button class="btn btn-primary form-control"><i class="el el-search"></i>查詢</button>
											</div>
										</div>
									</form>
									
								</div>							
							</div>
						</section>


						<section class="panel panel-featured-success panel-featured" role="query">
							<header class="panel-heading">
							<h3 class="panel-title"><i class="el el-list">查詢結果</i></h3>
							</header>						
							<div class="panel-body">
								<div class="table-responsive">
									<table class="table table-bordered table-striped table-condensed mb-none" id="tableData">
										<thead>
											<tr>
												<th class="text-center">租借編號</th>
												<th class="text-center">租借時間</th>
												<th class="text-center">開始時間</th>
												<th class="text-center">結束時間</th>
												<th class="text-center">資材編號</th>
												<th class="text-center">數量</th>
												<th class="text-center">計畫代號</th>
												<th class="text-center"></th>
											</tr>
										</thead>
										<tbody>
										</tbody>
									</table>
<script id="detail-template" type="text/x-handlebars-template">
											{{#each ds}}
											<tr>
												<td>{{bookingItemId}}</td>
												<td>{{bookingItemDate}}</td>
												<td>{{startTime}}</td>
												<td>{{endTime}}</td>
												<td>{{itemId}}</td>
												<td>{{qty}}</td>
												<td>{{projId}}</td>
												<td class="text-center">

												<button type="button" class="btn-sm btn btn-info" onclick="ui.showConfirmDialog('CONFIRM_UPDATE','{{bookingItemId}}');"><i class="el el-search"></i>歸還</button>
												
												</td>
											</tr>
											{{/each}}
</script>
									
									<br/>
									<div class="col-md-offset-5 col-md-7 text-right">
										<form class="form-inline">
											<div class="form-group">
												<label >跳至</label>
												<select class="form-control" id="qpage">
												</select>
												<label >，共有<span id="totalRecord">0</span>筆資料</label>
											</div>
										</form>
									</div>
									<!-- </div> -->
								</div>
							</div>
						</section>
						<!-- end 查詢 -->
					<!-- end: page -->
				</section>
			</div>

		</section>

<%@include file="../include/includeFooterJs.jsp"%>
		<script src="js/returning/returningItem.js?<c:out value="${applicationScope.js_version}"/>"></script>
	</body>
</html>