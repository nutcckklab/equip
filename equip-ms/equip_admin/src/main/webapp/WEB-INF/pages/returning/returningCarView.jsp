<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!doctype html>
 <!-- sidebar-light -->
<html class="fixed sidebar-light" lang="zh-Hant-TW">
<%@include file="../include/includeHeader.jsp"%>
<head>
<style>
tr{
	height:130%;
}
</style>
</head>
	<body>
		<section class="body">

			<!-- start: header -->
<%@include file="../include/includeTopHeader.jsp"%>
			<!-- end: header -->

			<div class="inner-wrapper">
				<!-- start: sidebar -->
<%@include file="../include/includeMenu.jsp"%>
				<!-- end: sidebar -->

				<section role="main" class="content-body">
					<header class="page-header">
						<h2>汽車歸還</h2>
					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="index.html">
										<i class="fa fa-home"></i>
									</a>
								</li>
								<li><span>歸還</span></li>
								<li><span>汽車歸還</span></li>
							</ol>
							&nbsp;&nbsp;&nbsp;
						</div>
					</header>

					<!-- start: page -->
						<!-- start 借車area -->
						<section class="panel panel-featured-success panel-featured" role="query">
							<header class="panel-heading">
								<h3 class="panel-title"><i class="el el-list">借車資訊</i></h3>
							</header>	
							<div class="panel-body" >
								<form id="queryForm">
								    <table class="table">
								    	<tr>
								    		<td style="border:none;">預約日期：<span id="bookingDate"></span></td>
								    		<td style="border:none;">計畫代號：<span id="projId"></span></td>
								    	</tr>
								    	<tr>
								    		<td>開始時間：<span id="startTime"></span></td>
								    		<td>結束時間：<span id="endTime"></span></td>
								    	</tr>
								    	<tr>
								    		<td>費用：<span id="cost"></span></td>
								    		<td>目的地：<span id="destination"></span></td>
								    	</tr>
								    </table>
							    </form>
							</div>
												
						</section>
						<!-- end 借車area -->
					
						<!-- 歸還 area -->
						<section class="panel panel-featured-info panel-featured" role="update"  style="display:none;">
							<header class="panel-heading">
								<h3 class="panel-title"><i class="el el-edit">汽車歸還</i></h3>
							</header>						
							<div class="panel-body">
								<form class="form-horizontal form-bordered" id="updateForm">
									<div class="form-group">
										<label class="col-md-2 control-label">結束里程數</label>
										<div class="col-md-6">
											<input name="endMilage" class="form-control" type="text">
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-2 control-label">設置地點</label>
										<div class="col-md-6">
											<input name="carLocation" class="form-control" type="text" >
										</div>
									</div>
									
									<div class="text-center">
										<!-- <button class="btn btn-default btn-lg" onclick="ui.gotoMode('MODE_UPDATE_CANCEL');"><i class="el el-return-key"></i>取消退出</button> -->
										<button class="btn btn-info btn-lg" onclick="ui.showConfirmDialog('CONFIRM_UPDATE');"><i class="el el-ok"></i>確定送出</button>
									</div>						
								</form>
							</div>
						</section>
						<!-- end 歸還 area -->						

					<!-- end: page -->
				</section>
			</div>

		</section>

<%@include file="../include/includeFooterJs.jsp"%>
		<script src="js/returning/returningCar.js?<c:out value="${applicationScope.js_version}"/>"></script>
	</body>
</html>