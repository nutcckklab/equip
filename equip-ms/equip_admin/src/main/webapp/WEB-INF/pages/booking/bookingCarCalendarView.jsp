<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html class="fixed sidebar-light" lang="zh-Hant-TW">
<%@include file="../include/includeHeader.jsp"%>
<head>
<style>
.fc-future{
	cursor:pointer;
}
.fc-today{
	cursor:pointer;
}
.fc-future:hover{
    background:#f6f6f6;
}
.fc-past{
	background-color:#d1d1d1;
}
</style>
</head>
	<body>
		<section class="body">

			<!-- start: header -->
			<%@include file="../include/includeTopHeader.jsp"%>
			<!-- end: header -->

			<div class="inner-wrapper">
				<!-- start: sidebar -->
				<%@include file="../include/includeMenu.jsp"%>
				<!-- end: sidebar -->

				<section role="main" class="content-body" >
					<header class="page-header">
						<h2>汽車預約月曆</h2>
					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="index.html">
										<i class="fa fa-home"></i>
									</a>
								</li>
								<li><span>預約</span></li>
								<li><span>汽車預約</span></li>
							</ol>
							&nbsp;&nbsp;&nbsp;
						</div>
					</header>

					<!-- start: page -->
					<section class="panel" role="calendar">
						<div class="panel-body" id="calendarSection">
							<div class="row">
								<div class="col-md-12">
									<div id="calendar" class="fc fc-unthemed fc-ltr"></div>
								</div>
							</div>
						</div>
					</section>
					<!-- end: page -->
					<!-- 新增 area -->
					<section class="panel panel-featured-primary panel-featured" role="create" style="display:none;">
						<header class="panel-heading">
							<h3 class="panel-title"><i class="el el-file-new">汽車預約</i></h3>
						</header>						
						<div class="panel-body">
							<form class="form-horizontal form-bordered" id="createForm" >
								<div class="form-group">
									<label class="col-md-2 control-label">預約日期</label>
									<div class="col-md-6">
										<input name="bookingDate" class="form-control" type="date" readonly>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label">計畫代號</label>
									<div class="col-md-6">
							            <select name="projId" id="projId"></select>
										<!--<input name=projId class="form-control" type="text">-->
									</div>
								</div>
								
								<div class="form-group">
									<label class="col-md-2 control-label">開始時間</label>
									<div class="col-md-6">
										<select name="startTime" id="startTime"></select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label">結束時間</label>
									<div class="col-md-6">
										<select name="endTime" id="endTime"></select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label">費用</label>
									<div class="col-md-6">
										<input name="cost" class="form-control" type="text">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label">目的地</label>
									<div class="col-md-6">
										<input name="destination" class="form-control" type="text">
									</div>
								</div>
								
								<div class="text-center">
									<button class="btn btn-default btn-lg" onclick="ui.gotoMode('MODE_CREATE_CANCEL');"><i class="el el-return-key"></i>取消退出</button>
									<button class="btn btn-primary btn-lg" onclick="ui.showConfirmDialog('CONFIRM_CREATE');"><i class="el el-ok"></i>確定預約</button>										
								</div>						
							</form>
						</div>
					</section>
					<!-- end 新增area -->
				</section>
			</div>
		</section>
		
		

		<%@include file="../include/includeFooterJs.jsp"%>
		<script src="js/booking/bookingCarCalendar.js?<c:out value="${applicationScope.js_version}"/>"></script>
	
</body></html>