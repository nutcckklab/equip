<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!doctype html>
 <!-- sidebar-light -->
<html class="fixed sidebar-light" lang="zh-Hant-TW">
<%@include file="../include/includeHeader.jsp"%>
	<body>
		<section class="body">

			<!-- start: header -->
<%@include file="../include/includeTopHeader.jsp"%>
			<!-- end: header -->

			<div class="inner-wrapper">
				<!-- start: sidebar -->
<%@include file="../include/includeMenu.jsp"%>
				<!-- end: sidebar -->

				<section role="main" class="content-body">
					<header class="page-header">
						<h2>統計報表</h2>
					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="index.html">
										<i class="fa fa-home"></i>
									</a>
								</li>
								<li><span>統計報表</span></li>
							</ol>
							&nbsp;&nbsp;&nbsp;
						</div>
					</header>

					<!-- start: page -->						
						<!-- 查詢 area -->
						<section class="panel panel-featured-primary panel-featured" role="query">
							<header class="panel-heading">
							<h3 class="panel-title"><i class="el el-search">統計條件</i></h3>
							</header>						
							<div class="panel-body">
								<div class="row">
									<form id="queryForm" >
										<input name="page" type="hidden">
										<div class="form-inline">
											<div class="form-group" style="margin:0 20px 0 20px">
												<label class="control-label" >起訖時間：</label>
                    							<input name="startTime" id="startTime" type='date' class="form-control" />
									            <script type="text/javascript">
										        </script>
										    	<label class="control-label" >至</label>
                    							<input name="endTime" id="endTime" type='date' class="form-control" />
									            <div class="form-group">
													<label class="control-label" style="margin-left:10px;">類型：</label>
													<select name="type" id="type">
														<option value="car_proj">計畫借車統計</option>
														<option value="car_depart">部門借車統計</option>
														<option value="car_user">個人借車統計</option>
														<option value="car_notReturn">個人未填還車單統計</option>
														<option value="item_stock">資材庫存統計</option>
														<option value="item_bookReturn" onselect="">資材借還統計</option>
													</select>
													<select name="item_type" id="item_type" style="display:none;">
														<option value="all">全部</option>
														<option value="book">借出</option>
														<option value="return">歸還</option>
													</select>
												</div>
												<button class="btn btn-primary form-control" style="margin-left:10px;"><i class="el el-search"></i>查詢</button>
											</div>
										</div>
									</form>
								</div>							
							</div>
						</section>
						<section class="panel panel-featured-success panel-featured" role="query">
							<header class="panel-heading">
							<h3 class="panel-title"><i class="el el-list">統計報表</i></h3>
							</header>						
							<div class="panel-body">
								<div class="table-responsive">
									 <table class="table table-bordered table-striped table-condensed mb-none" id="tableData">
										<thead>
										</thead>
										<tbody>
										</tbody>
									 </table>
									 <script class="detail-template" id="car_proj" type="text/x-handlebars-template">
											{{#each ds}}
												<tr>
													<td>{{projId}}</td>
													<td>{{count}}</td>
												<tr>
											{{/each}}
									</script>
									<script class="detail-template" id="car_depart" type="text/x-handlebars-template">
											{{#each ds}}
												<tr>
													<td>{{departName}}</td>
													<td>{{count}}</td>
												<tr>
											{{/each}}
									</script>
									<script class="detail-template" id="car_user" type="text/x-handlebars-template">
											{{#each ds}}
												<tr>
													<td>{{name}}</td>
													<td>{{count}}</td>
												<tr>
											{{/each}}
									</script>
									<script class="detail-template" id="car_notReturn" type="text/x-handlebars-template">
											{{#each ds}}
												<tr>
													<td>{{name}}</td>
													<td>{{count}}</td>
												<tr>
											{{/each}}
									</script>
									<script class="detail-template" id="item_stock" type="text/x-handlebars-template">
											{{#each ds}}
												<tr>
													<td>{{itemName}}</td>
													<td>{{quantity}}</td>
												<tr>
											{{/each}}
									</script>
									<script class="detail-template" id="item_bookReturn" type="text/x-handlebars-template">
											{{#each ds}}
												<tr>
													<td>{{date}}</td>
													<td>{{itemName}}</td>
													<td>{{qty}}</td>
												<tr>
											{{/each}}
									</script>
									<script id="th-template" type="text/x-handlebars-template">
											<tr>	
												{{#each th}}
													<th class="text-center">{{thContext}}</th>
												{{/each}}
											</tr>										
									</script>
									
									<br/>
									<div class="col-md-offset-5 col-md-7 text-right">
										<form class="form-inline">
											<div class="form-group">
												<label >跳至</label>
												<select class="form-control" id="qpage">
												</select>
												<label >，共有<span id="totalRecord">0</span>筆資料</label>
											</div>
										</form>
									</div>
									<!-- </div> -->
								</div>
							</div>
						</section>
						<!-- end 查詢 -->
					<!-- end: page -->
				</section>
			</div>

		</section>

<%@include file="../include/includeFooterJs.jsp"%>
		<script src="js/report/report.js?<c:out value="${applicationScope.js_version}"/>"></script>
	</body>
</html>