package com.bean;


import itri.group.param.Pa;

import java.io.File;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.http.HttpServlet;


public class SysListener extends HttpServlet implements ServletContextListener{

	private static final long serialVersionUID = 1L;

	@Override
    public void contextDestroyed(ServletContextEvent arg0) {
    }

    @Override
    public void contextInitialized(ServletContextEvent sce) {
    	
        sce.getServletContext().setAttribute("js_version", Pa.JS_VERSION);
        sce.getServletContext().setAttribute("lib_link", Pa.CDN_ADMIN_LIB_LINK);
    }

}