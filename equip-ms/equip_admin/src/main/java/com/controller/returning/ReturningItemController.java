package com.controller.returning;

import itri.group.param.Pa;
import itri.group.param.RsMsg;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;



import com.dao.BookingItemDocDao;
import com.google.gson.Gson;
import com.security.SecurityController;

@Controller
@RequestMapping(value="returningItem")
public class ReturningItemController extends SecurityController{
	
	@Autowired
	Gson gson;
	
	
	@Autowired
	BookingItemDocDao bookingItemDocDao;
	
	@RequestMapping()
	public String shipDocAssign(ModelMap model) {
	    return "returning/returningItemView";
	}
	
	@RequestMapping(value="/find",method = RequestMethod.GET)
	public @ResponseBody String find(
			@RequestParam(value="userId",defaultValue="") String userId,
			@RequestParam(value="page",defaultValue="") String page,
			ModelMap model) {
		return gson.toJson(new RsMsg("success",	bookingItemDocDao.find("a12345",page)));
	}
	
	@RequestMapping(value="/find_return",method = RequestMethod.GET)
	public @ResponseBody String find_return(
			@RequestParam(value="userId",defaultValue="") String userId,
			@RequestParam(value="page",defaultValue="") String page,
			ModelMap model) {
		return gson.toJson(new RsMsg("success",	bookingItemDocDao.find_return("a12345",page)));
	}
	
	@RequestMapping(value="/findPage",method = RequestMethod.GET)
	public @ResponseBody String findPage(
			@RequestParam(value="projId",defaultValue="") String projId,
			ModelMap model) {
		return gson.toJson(new RsMsg("success",	bookingItemDocDao.findPage(projId)));
	}
	
	@RequestMapping(value="/findPage_return",method = RequestMethod.GET)
	public @ResponseBody String findPage_return(
			@RequestParam(value="projId",defaultValue="") String projId,
			ModelMap model) {
		return gson.toJson(new RsMsg("success",	bookingItemDocDao.findPage_return(projId)));
	}
	
	@RequestMapping(value="/update",method = RequestMethod.POST)
	public @ResponseBody Object update(
			@RequestParam(value="bookingItemId",defaultValue="") String bookingItemId,			
			@RequestParam(value="quantity",defaultValue="") String quantity,
			@RequestParam(value="qty",defaultValue="") String qty,
			ModelMap model) {
		try {
			bookingItemDocDao.update(getUpdateDate(), getUpdateDate(), "a12345", bookingItemId, quantity, qty);
			return Pa.success;
		} catch (Exception e) {
			e.printStackTrace();
			return new RsMsg("error", e);
		}
	}
}
