package com.controller.returning;

import itri.group.param.Pa;
import itri.group.param.RsMsg;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dao.BookingCarDocDao;
import com.dao.ProjDao;
import com.google.gson.Gson;
import com.security.SecurityController;

/******************************************
 * ReturningCarController.java
 * ReturningCarDao.java
 * returningCarView.js
 * returningCarView.jsp
 *
 * @author  create by 2016/12/07
 *******************************************/

@Controller
@RequestMapping(value="returningCar")
public class ReturningCarController extends SecurityController{
	
	@Autowired
	Gson gson;
	
	
	@Autowired
	BookingCarDocDao bookingCarDocDao;
	
	@RequestMapping()
	public String shipDocAssign(ModelMap model) {
	    return "returning/returningCarView";
	}
	
	@RequestMapping(value="/find",method = RequestMethod.GET)
	public @ResponseBody String find(ModelMap model) {
		return gson.toJson(new RsMsg("success",	bookingCarDocDao.find(Pa.userId,"","","1",false)));
	}
	
	@RequestMapping(value="/update",method = RequestMethod.POST)
	public @ResponseBody Object update(
			@RequestParam(value="bookingCarId",defaultValue="") String bookingCarId,
			@RequestParam(value="endMilage",defaultValue="") String endMilage,
			@RequestParam(value="carLocation",defaultValue="") String carLocation,
			ModelMap model) {
		try {
			bookingCarDocDao.update(bookingCarId,getUpdateDate(),endMilage,carLocation,getUpdateDate(),Pa.userId);
			return Pa.success;
		} catch (Exception e) {
			e.printStackTrace();
			return new RsMsg("error", e);
		}
	}		


	
}
