package com.controller;

import itri.group.param.RsMsg;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dao.BookingCarDocDao;
import com.google.gson.Gson;
import com.security.SecurityController;

@Controller
@RequestMapping(value="index")
public class IndexController extends SecurityController{

	@Autowired
	Gson gson;
	
	
	@Autowired
	BookingCarDocDao bookingCarDocDao;
	
	@RequestMapping()
	public String shipDocAssign(ModelMap model) {
	    return "index/indexView";
	}
	
	@RequestMapping(value="/find",method = RequestMethod.GET)
	public @ResponseBody String find(ModelMap model) {
		return gson.toJson(new RsMsg("success",	bookingCarDocDao.index()));
	}
}
