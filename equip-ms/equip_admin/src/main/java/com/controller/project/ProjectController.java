package com.controller.project;

import itri.group.param.Pa;
import itri.group.param.RsMsg;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dao.ProjDao;
import com.google.gson.Gson;
import com.security.SecurityController;

/******************************************
 * ProjectController.java
 * ProjectDao.java
 * project.js
 * projectView.jsp
 *
 * @author  create by 2016/12/07
 *******************************************/

@Controller
@RequestMapping(value="project")
public class ProjectController extends SecurityController{
	
	@Autowired
	Gson gson;
	
	
	@Autowired
	ProjDao projDao;
	
	@RequestMapping()
	public String shipDocAssign(ModelMap model) {
	    return "project/projectView";
	}
	
	
	
	@RequestMapping(value="/find",method = RequestMethod.GET)
	public @ResponseBody String find(
			@RequestParam(value="projId",defaultValue="") String projId,
			@RequestParam(value="page",defaultValue="") String page,
			ModelMap model) {
		return gson.toJson(new RsMsg("success",	projDao.find(projId,page)));
	}
	
	@RequestMapping(value="/findPage",method = RequestMethod.GET)
	public @ResponseBody String findPage(
			@RequestParam(value="projId",defaultValue="") String projId,
			ModelMap model) {
		return gson.toJson(new RsMsg("success",	projDao.findPage(projId)));
	}
    
	@RequestMapping(value="/update",method = RequestMethod.POST)
	public @ResponseBody Object update(
			@RequestParam(value="projId",defaultValue="") String projId,
			@RequestParam(value="startDate",defaultValue="") String startDate,
			@RequestParam(value="endDate",defaultValue="") String endDate,
			@RequestParam(value="projBudget",defaultValue="") String projBudget,
			ModelMap model) {
		try {
			projDao.update(projId, startDate, endDate,projBudget,getUpdateDate(),"a12345");
			return Pa.success;
		} catch (Exception e) {
			e.printStackTrace();
			return new RsMsg("error", e);
		}
	}		
	
	@RequestMapping(value="/create",method = RequestMethod.POST)
	public @ResponseBody Object create(
			@RequestParam(value="projId",defaultValue="") String projId,
			@RequestParam(value="startDate",defaultValue="") String startDate,
			@RequestParam(value="endDate",defaultValue="") String endDate,
			@RequestParam(value="projBudget",defaultValue="") String projBudget,
			ModelMap model) {
		try {
			projDao.create(projId, startDate, endDate, projBudget,getUpdateDate(),"a12345");
			return Pa.success;
		} catch (Exception e) {
			e.printStackTrace();
			return new RsMsg("error", e);
		}
	}		
}
