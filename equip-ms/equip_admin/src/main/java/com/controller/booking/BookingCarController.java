package com.controller.booking;

import itri.group.param.Pa;
import itri.group.param.RsMsg;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dao.BookingCarDocDao;
import com.dao.ProjDao;
import com.google.gson.Gson;
import com.security.SecurityController;

/******************************************
 * BookingCarController.java
 * BookingCarDao.java
 * placeAssign.js
 * bookingCar.jsp
 *
 * @author  create by 2016/12/10
 *******************************************/

@Controller
@RequestMapping(value="bookingCar")
public class BookingCarController extends SecurityController{
	
	@Autowired
	Gson gson;
	
	@Autowired
	BookingCarDocDao bookingCarDocDao;
	
	@Autowired
	ProjDao projDao;
	
	@RequestMapping()
	public String shipDocAssign(ModelMap model) {
		return "booking/bookingCarCalendarView";
	}
	
	@RequestMapping(value="/find",method = RequestMethod.GET)	
	public @ResponseBody String find(ModelMap model) {
		return gson.toJson(new RsMsg("success",	bookingCarDocDao.find("","","","",false)));
	}
	
	@RequestMapping(value="/findCanUseProj",method = RequestMethod.GET)	
	public @ResponseBody String findCanUseProj(ModelMap model) {
		return gson.toJson(new RsMsg("success",	projDao.findCanUseProj(getUpdateDate())));
	}
	
	@RequestMapping(value="/create",method = RequestMethod.POST)
	public @ResponseBody Object create(
			@RequestParam(value="projId",defaultValue="") String projId,
			@RequestParam(value="bookingDate",defaultValue="") String bookingDate,
			@RequestParam(value="startTime",defaultValue="") String startTime,
			@RequestParam(value="endTime",defaultValue="") String endTime,
			@RequestParam(value="cost",defaultValue="") String cost,
			@RequestParam(value="destination",defaultValue="") String destination,
			ModelMap model) {
		try {
			bookingCarDocDao.create(bookingDate, startTime,endTime,"a12345",projId,cost,destination,getUpdateDate(),"a12345");
			return Pa.success;
		} catch (Exception e) {
			e.printStackTrace();
			return new RsMsg("error", e);
		}
	}		
}
