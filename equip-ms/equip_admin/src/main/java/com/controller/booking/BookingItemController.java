package com.controller.booking;

import itri.group.param.Pa;
import itri.group.param.RsMsg;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dao.BookingItemDocDao;
import com.dao.ItemStockDao;
import com.google.gson.Gson;
import com.security.SecurityController;

@Controller
@RequestMapping(value="bookingItem")
public class BookingItemController extends SecurityController{
	
	@Autowired
	Gson gson;
	
	@Autowired
	BookingItemDocDao bookingItemDocDao;
	
	@Autowired
	ItemStockDao itemStockDao;

	@RequestMapping()
	public String shipDocAssign(ModelMap model) {
	    return "booking/bookingItemView";
	}
	
	@RequestMapping(value="/find",method = RequestMethod.GET)
	public @ResponseBody String find(
			@RequestParam(value="itemId",defaultValue="") String itemId,
			@RequestParam(value="page",defaultValue="") String page,
			ModelMap model) {
		return gson.toJson(new RsMsg("success",	bookingItemDocDao.find(itemId,page)));
	}
	
	@RequestMapping(value="/findPage",method = RequestMethod.GET)
	public @ResponseBody String findPage(
			@RequestParam(value="itemId",defaultValue="") String itemId,
			ModelMap model) {
		return gson.toJson(new RsMsg("success",	bookingItemDocDao.findPage(itemId)));
	}
	
	@RequestMapping(value="/create",method = RequestMethod.POST)
	public @ResponseBody Object create(
			@RequestParam(value="bookingDate",defaultValue="") String bookingDate,
			@RequestParam(value="startTime",defaultValue="") String startTime,
			@RequestParam(value="endTime",defaultValue="") String endTime,
			@RequestParam(value="itemId",defaultValue="") String itemId,
			@RequestParam(value="qty",defaultValue="") String qty,
			@RequestParam(value="userId",defaultValue="") String userId,
			@RequestParam(value="projId",defaultValue="") String projId,
			ModelMap model) {
		try {
			bookingItemDocDao.create(bookingDate, startTime,endTime,itemId,qty,"a12345",projId,getUpdateDate(),"a12345");
			return Pa.success;
		} catch (Exception e) {
			e.printStackTrace();
			return new RsMsg("error", e);
		}
	}
	
	@RequestMapping(value="/update",method = RequestMethod.POST)
	public @ResponseBody Object update(
			@RequestParam(value="itemId",defaultValue="") String itemId,
			@RequestParam(value="qty",defaultValue="") String qty,
			ModelMap model) {
		try {
			itemStockDao.update(itemId,qty,getUpdateDate(),"a12345");
			return Pa.success;
		} catch (Exception e) {
			e.printStackTrace();
			return new RsMsg("error", e);
		}
	}

}
