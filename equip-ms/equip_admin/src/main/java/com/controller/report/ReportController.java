package com.controller.report;

import itri.group.param.Pa;
import itri.group.param.RsMsg;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dao.ProjDao;
import com.dao.BookingCarDocDao;
import com.dao.BookingItemDocDao;
import com.dao.ItemStockDao;
import com.google.gson.Gson;

/******************************************
 * ProjectController.java
 * ProjectDao.java
 * project.js
 * projectView.jsp
 *
 * @author  create by 2016/12/07
 *******************************************/

@Controller
@RequestMapping(value="report")
public class ReportController {
	@Autowired
	Gson gson;
		
	@Autowired
	BookingCarDocDao bookingCarDocDao;
	
	@Autowired
	BookingItemDocDao bookingItemDocDao;
	
	@Autowired
	ItemStockDao itemStockDao;
	
	@RequestMapping()
	public String shipDocAssign(ModelMap model) {
	    return "report/reportView";
	}

	
	@RequestMapping(value="/findCarProj",method = RequestMethod.GET)
	public @ResponseBody String findCarProj(
			@RequestParam(value="startTime",defaultValue="") String startTime,
			@RequestParam(value="endTime",defaultValue="") String endTime,
			@RequestParam(value="page",defaultValue="") String page,
			ModelMap model) {
		return gson.toJson(new RsMsg("success",	bookingCarDocDao.find("",startTime,endTime,page,true)));
	}
	
	@RequestMapping(value="/findCarDepart",method = RequestMethod.GET)
	public @ResponseBody String findCarDepart(
			@RequestParam(value="startTime",defaultValue="") String startTime,
			@RequestParam(value="endTime",defaultValue="") String endTime,
			@RequestParam(value="page",defaultValue="") String page,
			ModelMap model) {
		return gson.toJson(new RsMsg("success",	bookingCarDocDao.findCarDepart(startTime,endTime,page)));
	}
	
	@RequestMapping(value="/findCarUser",method = RequestMethod.GET)
	public @ResponseBody String findCarUser(
			@RequestParam(value="startTime",defaultValue="") String startTime,
			@RequestParam(value="endTime",defaultValue="") String endTime,
			@RequestParam(value="page",defaultValue="") String page,
			ModelMap model) {
		return gson.toJson(new RsMsg("success",	bookingCarDocDao.findCarUser(startTime,endTime,page)));
	}
	
	@RequestMapping(value="/findCarNotReturn",method = RequestMethod.GET)
	public @ResponseBody String findCarNotReturn(
			@RequestParam(value="startTime",defaultValue="") String startTime,
			@RequestParam(value="endTime",defaultValue="") String endTime,
			@RequestParam(value="page",defaultValue="") String page,
			ModelMap model) {
		return gson.toJson(new RsMsg("success",	bookingCarDocDao.findCarNotReturn(startTime,endTime,page)));
	}
	
	@RequestMapping(value="/findItemStock",method = RequestMethod.GET)
	public @ResponseBody String findItemStock(
			@RequestParam(value="page",defaultValue="") String page,
			ModelMap model) {
		return gson.toJson(new RsMsg("success",	itemStockDao.find(page)));
	}
	
	@RequestMapping(value="/findItemBookReturn",method = RequestMethod.GET)
	public @ResponseBody String findItemBookReturn(
			@RequestParam(value="startTime",defaultValue="") String startTime,
			@RequestParam(value="endTime",defaultValue="") String endTime,
			@RequestParam(value="item_type",defaultValue="") String item_type,
			@RequestParam(value="page",defaultValue="") String page,
			ModelMap model) {
		return gson.toJson(new RsMsg("success",	bookingItemDocDao.findItemBookReturn(startTime,endTime,item_type,page)));
	}

}
