package com.dao;

import itri.group.param.Pa;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ItemStockDao {
	
	@Autowired
	JdbcTemplate jdbcTemplate;

	public Object find(String page){
		StringBuilder sql= new StringBuilder("select item.itemName,item_stock.quantity from item,item_stock where item.itemId=item_stock.itemId ");
		if(StringUtils.isNotBlank(page)){
			sql.append(String.format("limit  %s,%d ",(Integer.parseInt(page)-1)*Pa.PAGE_NUM,Pa.PAGE_NUM));
		}else{
			sql.append(String.format("limit  %d ",Pa.PAGE_NUM));
		}
		System.out.println(sql);
		List list=jdbcTemplate.queryForList(sql.toString());
		return list;
	}
	
	public Object findItemStockPage(){
		StringBuilder sql= new StringBuilder("select count(itemId) as count from item_stock");
		System.out.println(sql);
		Map<String,Object> rs=jdbcTemplate.queryForList(sql.toString()).get(0);
		rs.put("page_num", Pa.PAGE_NUM);
		return rs;
	}
	
	@Transactional
	public void update(String itemId,String qty,String updateDate,String updator){
		String condi="update item_stock set quantity=quantity-?,updateDate=?,updator=? where itemId=?";
		jdbcTemplate.update(condi,new Object[]{Integer.parseInt(qty),updateDate,updator,itemId});
		System.out.println(condi);
	}
}
