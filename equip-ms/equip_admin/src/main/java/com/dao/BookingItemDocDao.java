package com.dao;

import itri.group.param.Pa;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class BookingItemDocDao {
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	public Object find(String itemId,String page){
		
		
		StringBuilder sql= new StringBuilder("select item.*,item_stock.quantity from item,item_stock where item.itemId=item_stock.itemId ");
		//StringBuilder sql= new StringBuilder("select booking_item_doc.*, item_stock.quantity from booking_item_doc, item_stock where booking_item_doc.itemId = item_stock.itemId ");
		List query=new ArrayList();
		if(StringUtils.isNotBlank(itemId)){
			sql.append("and item.itemId like ? and ");
			query.add(itemId);
		}
		if(query.size()>0){
			sql.delete(sql.length()-4,	sql.length());
		}
		if(StringUtils.isNotBlank(page)){
			sql.append(String.format("limit  %s,%d ",(Integer.parseInt(page)-1)*Pa.PAGE_NUM,Pa.PAGE_NUM));
		}else{
			sql.append(String.format("limit  %d ",Pa.PAGE_NUM));
		}
		System.out.println(sql);
		List list=jdbcTemplate.queryForList(sql.toString(),query.toArray());
		return list;
		
	}
   
	public Object find_return(String userId,String page){
		
		StringBuilder sql= new StringBuilder("select booking_item_doc.*, item_stock.quantity from booking_item_doc, item_stock where booking_item_doc.itemId = item_stock.itemId and returnTime is null ");
		List query=new ArrayList();
		System.out.println(userId);
		if(StringUtils.isNotBlank(userId)){
			sql.append(" and booking_item_doc.userId like ? and ");
			query.add(userId);
		}
		if(query.size()>0){
			sql.delete(sql.length()-4,	sql.length());
		}
		if(StringUtils.isNotBlank(page)){
			sql.append(String.format(" limit  %s,%d ", (Integer.parseInt(page)-1)*Pa.PAGE_NUM, Pa.PAGE_NUM));
		}else{
			sql.append(String.format(" limit  %d ", Pa.PAGE_NUM));
		}
		System.out.println(sql);
		List list = jdbcTemplate.queryForList(sql.toString(), query.toArray());
		return list;
	}
	
	public Object findItemBookReturn(String startDate,String endDate,String item_type,String page){
		StringBuilder sql= new StringBuilder();
		List query=new ArrayList();
		System.out.println(item_type);
		if(item_type.equals("all")){
			sql.append("(");
		}
		if(item_type.equals("all") || item_type.equals("book")){
			sql.append("select booking_item_doc.bookingItemDate as date,booking_item_doc.returnTime,item.itemName,booking_item_doc.qty "
					+ "from booking_item_doc,item  where booking_item_doc.itemId=item.itemId ");
			if(item_type.equals("book"))
				sql.append("and booking_item_doc.returnTime IS NULL ");
			if(StringUtils.isNotBlank(startDate) && StringUtils.isNotBlank(endDate)){
				sql.append("and booking_item_doc.bookingItemDate between ? and ? ");
				query.add(startDate);
				query.add(endDate);
			}else if(StringUtils.isNotBlank(startDate) && StringUtils.isBlank(endDate)){
				sql.append("and booking_item_doc.bookingItemDate>? ");
				query.add(startDate);
			}else if(StringUtils.isNotBlank(endDate) && StringUtils.isBlank(startDate)){
				sql.append("and booking_item_doc.bookingItemDate<? ");
				query.add(endDate);
			}else{
				if(item_type.equals("all"))
				sql.append(") union all (");
			}
		}
		if(item_type.equals("all") || item_type.equals("return")){
			sql.append("select booking_item_doc.returnTime as date,booking_item_doc.returnTime,item.itemName,booking_item_doc.qty "
					+ "from booking_item_doc,item  where booking_item_doc.itemId=item.itemId ");
			if(item_type.equals("return"))
				sql.append("and booking_item_doc.returnTime IS NOT NULL ");
			if(StringUtils.isNotBlank(startDate) && StringUtils.isNotBlank(endDate)){
				sql.append("and booking_item_doc.returnTime between ? and ? ");
				query.add(startDate);
				query.add(endDate);
			}else if(StringUtils.isNotBlank(startDate) && StringUtils.isBlank(endDate)){
				sql.append("and booking_item_doc.returnTime>? ");
				query.add(startDate);
			}else if(StringUtils.isNotBlank(endDate) && StringUtils.isBlank(startDate)){
				sql.append("and booking_item_doc.returnTime<? ");
				query.add(endDate);
			}else{
				if(item_type.equals("all"))
					sql.append(") ");
			}
		}
		sql.append("order by date desc ");
		if(StringUtils.isNotBlank(page)){
			sql.append(String.format("limit %s,%d ",(Integer.parseInt(page)-1)*Pa.PAGE_NUM, Pa.PAGE_NUM));
		}else{
			sql.append(String.format("limit %d ",Pa.PAGE_NUM));
		}
		System.out.println(sql);
		List list=jdbcTemplate.queryForList(sql.toString(),query.toArray());
		return list;
	}
	
	public Object findPage(String itemId){

		StringBuilder sql= new StringBuilder("select count(*) as total from item ");
		
		List query=new ArrayList();
		if(StringUtils.isNotBlank(itemId)){
			sql.append("where itemId like ? and ");
			query.add(itemId);
		}
		if(query.size()>0){
			sql.delete(sql.length()-4,	sql.length());
		}
		System.out.println(sql);
		Map<String,Object> rs=jdbcTemplate.queryForList(sql.toString(), query.toArray()).get(0);
		rs.put("page_num", Pa.PAGE_NUM);
		return rs;
	}
	
	public Object findPage_return(String itemId){
		
		StringBuilder sql= new StringBuilder("select count(*) as total from booking_item_doc, item_stock where booking_item_doc.itemId = item_stock.itemId and returnTime is null ");
		List query=new ArrayList();
		if(StringUtils.isNotBlank(itemId)){
			sql.append(" and booking_item_doc.userId like ? and ");
			query.add(itemId);
		}
		if(query.size()>0){
			sql.delete(sql.length()-4,	sql.length());
		}
		System.out.println(sql);
		Map<String,Object> rs=jdbcTemplate.queryForList(sql.toString(),query.toArray()).get(0);
		rs.put("page_num", Pa.PAGE_NUM);
		return rs;
	}
	
	@Transactional
	public void create(String bookingItemDate,String startTime,String endTime,String itemId,String qty,String userId,String projId,String updateDate,String updator){
		String condi="insert booking_item_doc (bookingItemDate,startTime,endTime,itemId,qty,userId,projId,returnTime,updateDate,updator) values (?,?,?,?,?,?,?,?,?,?)";
		jdbcTemplate.update(condi,new Object[]{bookingItemDate,startTime,endTime,itemId,Integer.parseInt(qty),userId,projId,null,updateDate,updator});	
		System.out.println(condi);
	}
	
	@Transactional
	public void update(String returnTime, String updateDate, String updator, String bookingItemId, String quantity, String qty){
  	
    	String condi = "update itri_equip.booking_item_doc, itri_equip.item_stock "
    				 + "set booking_item_doc.returnTime = ?, booking_item_doc.updateDate = ?, booking_item_doc.updator = ?, item_stock.updateDate = ?, item_stock.updator = ?, item_stock.quantity = ? "
    				 + "where booking_item_doc.bookingItemId = ? and booking_item_doc.itemId = item_stock.itemId";
    	Integer sum=0;
    	sum = Integer.parseInt(quantity) + Integer.parseInt(qty);
    	System.out.println("TEST:sum = " + sum);
    	jdbcTemplate.update(condi, new Object[]{returnTime, updateDate, updator, updateDate, updator, sum, bookingItemId});
	}
	
}