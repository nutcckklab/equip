package com.dao;

import itri.group.param.Pa;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class BookingCarDocDao {
	@Autowired
	JdbcTemplate jdbcTemplate;

	public Object find(String userId,String startDate,String endDate,String page,Boolean GroupByProjId){
		StringBuilder sql;
		if(!GroupByProjId){
			sql= new StringBuilder("select * from booking_car_doc ");
		}else{
			sql= new StringBuilder("select projId,count(bookingCarId) as count from booking_car_doc ");
		}
		List query=new ArrayList();
		if(StringUtils.isNotBlank(userId)||GroupByProjId){
			sql.append("where ");
		}
		if(StringUtils.isNotBlank(userId)){
			sql.append("userId = ? and ");
			query.add(userId);
		}
		if(query.size()>0){
			sql.delete(sql.length()-4,	sql.length());
		}
		if(StringUtils.isNotBlank(startDate) && StringUtils.isNotBlank(endDate)){
			sql.append("bookingDate between ? and ? ");
			query.add(startDate);
			query.add(endDate);
		}else if(StringUtils.isNotBlank(startDate) && StringUtils.isBlank(endDate)){
			sql.append("bookingDate>? ");
			query.add(startDate);
		}else if(StringUtils.isNotBlank(endDate) && StringUtils.isBlank(startDate)){
			sql.append("bookingDate<? ");
			query.add(endDate);
		}else if(StringUtils.isBlank(startDate) && StringUtils.isBlank(endDate)){
			sql.delete(sql.length()-1,	sql.length());
		}
		
		if(GroupByProjId){
			sql.append("group by projId ");
			sql.append("order by projId ");
		}else{
			sql.append(" order by bookingDate desc ");
		}
		if(StringUtils.isNotBlank(page)){
			sql.append(String.format("limit %s,%d ",(Integer.parseInt(page)-1)*Pa.PAGE_NUM, Pa.PAGE_NUM));
		}
		System.out.println(sql);
		List list=jdbcTemplate.queryForList(sql.toString(),query.toArray());
		return list;
	}
	
	public Object findCarDepart(String startDate,String endDate,String page){
		StringBuilder sql= new StringBuilder("select department.departName,count(booking_car_doc.bookingCarId) as count from booking_car_doc,department,user where booking_car_doc.userId=user.userId and user.departId=department.departId ");
		List query=new ArrayList();
		if(StringUtils.isNotBlank(startDate) && StringUtils.isNotBlank(endDate)){
			sql.append("and booking_car_doc.bookingDate between ? and ? ");
			query.add(startDate);
			query.add(endDate);
		}else if(StringUtils.isNotBlank(startDate) && StringUtils.isBlank(endDate)){
			sql.append("and booking_car_doc.bookingDate>? ");
			query.add(startDate);
		}else if(StringUtils.isNotBlank(endDate) && StringUtils.isBlank(startDate)){
			sql.append("and booking_car_doc.bookingDate<? ");
			query.add(endDate);
		}
		sql.append("group by department.departId order by department.departId desc ");
		if(StringUtils.isNotBlank(page)){
			sql.append(String.format("limit  %s,%d ",(Integer.parseInt(page)-1)*Pa.PAGE_NUM,Pa.PAGE_NUM));
		}else{
			sql.append(String.format("limit  %d ",Pa.PAGE_NUM));
		}
		System.out.println(sql);
		List list=jdbcTemplate.queryForList(sql.toString(),query.toArray());
		return list;
	}
	
	public Object findCarUser(String startDate,String endDate,String page){
		StringBuilder sql= new StringBuilder("select user.name,count(booking_car_doc.bookingCarId) as count from booking_car_doc,user where booking_car_doc.userId=user.userId ");
		List query=new ArrayList();
		if(StringUtils.isNotBlank(startDate) && StringUtils.isNotBlank(endDate)){
			sql.append("and booking_car_doc.bookingDate between ? and ? ");
			query.add(startDate);
			query.add(endDate);
		}else if(StringUtils.isNotBlank(startDate) && StringUtils.isBlank(endDate)){
			sql.append("and booking_car_doc.bookingDate>? ");
			query.add(startDate);
		}else if(StringUtils.isNotBlank(endDate) && StringUtils.isBlank(startDate)){
			sql.append("and booking_car_doc.bookingDate<? ");
			query.add(endDate);
		}
		sql.append("group by booking_car_doc.userId order by booking_car_doc.userId desc ");
		if(StringUtils.isNotBlank(page)){
			sql.append(String.format("limit %s,%d ",(Integer.parseInt(page)-1)*Pa.PAGE_NUM,Pa.PAGE_NUM));
		}else{
			sql.append(String.format("limit %d ",Pa.PAGE_NUM));
		}
		System.out.println(sql);
		List list=jdbcTemplate.queryForList(sql.toString(),query.toArray());
		return list;
	}
	
	public Object findCarNotReturn(String startDate,String endDate,String page){
		StringBuilder sql= new StringBuilder("select user.name, count(*) as count from booking_car_doc,user where booking_car_doc.userId=user.userId and booking_car_doc.returnTime IS NULL ");
		List query=new ArrayList();
		if(StringUtils.isNotBlank(startDate) && StringUtils.isNotBlank(endDate)){
			sql.append("and booking_car_doc.bookingDate between ? and ? ");
			query.add(startDate);
			query.add(endDate);
		}else if(StringUtils.isNotBlank(startDate) && StringUtils.isBlank(endDate)){
			sql.append("and booking_car_doc.bookingDate>? ");
			query.add(startDate);
		}else if(StringUtils.isNotBlank(endDate) && StringUtils.isBlank(startDate)){
			sql.append("and booking_car_doc.bookingDate<? ");
			query.add(endDate);
		}
		sql.append("group by user.userId order by user.userId desc ");
		if(StringUtils.isNotBlank(page)){
			sql.append(String.format("limit  %s,%d ",(Integer.parseInt(page)-1)*Pa.PAGE_NUM,Pa.PAGE_NUM));
		}else{
			sql.append(String.format("limit  %d ",Pa.PAGE_NUM));
		}
		System.out.println(sql);
		List list=jdbcTemplate.queryForList(sql.toString(),query.toArray());
		return list;
	}
   /*
	public Object findPage(String projId){

		StringBuilder sql= new StringBuilder("select count(*) as total from proj ");
		List query=new ArrayList();
		if(StringUtils.isNotBlank(projId)){
			sql.append("where projId like ? and ");
			query.add(projId);
		}
		if(query.size()>0){
			sql.delete(sql.length()-4,	sql.length());
		}
		System.out.println(sql);
		Map<String,Object> rs=jdbcTemplate.queryForList(sql.toString(),query.toArray()).get(0);
		rs.put("page_num", Pa.PAGE_NUM);
		return rs;
	} */
	
	@Transactional
	public void update(String bookingCarId,String returnTime,String endMilage,String carLocation,String updateDate,String updator){
    	String condi="update booking_car_doc set returnTime=?,endMilage=?,carLocation=?,updateDate=?,updator=? where bookingCarId=?";
    	jdbcTemplate.update(condi,new Object[]{returnTime,endMilage,carLocation,updateDate,updator,bookingCarId});
		System.out.println(condi);

	}
	
	@Transactional
	public void create(String bookingDate,String startTime,String endTime,String userId,String projId,String cost,String destination,String updateDate,String updator){
		String condi="insert booking_car_doc (bookingDate,startTime,endTime,userId,projId,cost,destination,returnTime,endMilage,carLocation,updateDate,updator) values (?,?,?,?,?,?,?,?,?,?,?,?)";
		jdbcTemplate.update(condi,new Object[]{bookingDate,startTime,endTime,userId,projId,cost,destination,null,null,null,updateDate,updator});	
	}
	
	@Transactional
	public Object index(){
		StringBuilder sql= new StringBuilder("select returnTime, endMilage, carLocation from itri_equip.booking_car_doc order by returnTime desc limit 1 ");
		List list=jdbcTemplate.queryForList(sql.toString());
		return list;
	}
	
}