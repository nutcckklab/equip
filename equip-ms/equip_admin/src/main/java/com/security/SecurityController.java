package com.security;

import java.util.List;
import java.util.Map;

import jodd.datetime.JDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;

import com.google.gson.annotations.Until;

public class SecurityController {
	
	@Autowired
	UserSettingDao userSettingDao; 
	
	public String getUpdator(){
		if(SecurityContextHolder.getContext()==null||SecurityContextHolder.getContext().getAuthentication()==null){
			return "tim";
		}else{
			ManageUser user = (ManageUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			return user.getUsername();
		}
	}
	
	public String getUpdateDate(){
		return new JDateTime().toString("YYYYMMDDhhmmss");
	}
	
	//取得有關聯的餐廳
	public List<Map<String,Object>> getUpRelRest(){
		return userSettingDao.findRelRest(getUpdator());
	}
}
