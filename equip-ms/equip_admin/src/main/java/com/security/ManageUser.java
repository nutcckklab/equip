package com.security;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

public class ManageUser implements Serializable,UserDetails {
	
	private static final long serialVersionUID = 1L;
	
	private String phone;
	private String userName,email,zipCode,addr,sex,zipCodeDisName,sexDisName,zipCodeParentDisName;
	
	private String password;
	private String type;

	public Collection<? extends GrantedAuthority> getAuthorities() {
		List<GrantedAuthority> list=new ArrayList();
		list.add(new SimpleGrantedAuthority(type));
		return list;
	}

	public String getPassword() {
		return password;
	}

	public String getUsername() {
		return getUserName();
	}
	

	public boolean isAccountNonExpired() {
		return true;
	}

	public boolean isAccountNonLocked() {
		// TODO Auto-generated method stub
		return true;
	}

	public boolean isCredentialsNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return true;
	}


	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getAddr() {
		return addr;
	}

	public void setAddr(String addr) {
		this.addr = addr;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getZipCodeDisName() {
		return zipCodeDisName;
	}

	public void setZipCodeDisName(String zipCodeDisName) {
		this.zipCodeDisName = zipCodeDisName;
	}

	public String getSexDisName() {
		return sexDisName==null||sexDisName.equals("M")?"先生":"小姐";
	}

	public String getZipCodeParentDisName() {
		return zipCodeParentDisName;
	}

	public void setZipCodeParentDisName(String zipCodeParentDisName) {
		this.zipCodeParentDisName = zipCodeParentDisName;
	}
	
	
}
