package com.security;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.StandardPasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class SecurityManager implements UserDetailsService {
	
	@Autowired
	StandardPasswordEncoder encoder;
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException, DataAccessException {
//		System.out.println(passwordEncoder.encode("test"));
		
		try {
			String[] ss=username.split("_");
			String sql="select phone ,password,customerDisName as userName from customer where phone='"+ss[1]+"' and customerDisName='"+ss[2]+"'";
			String type="clientAdmin";
			List<ManageUser> list=jdbcTemplate.query(sql, new BeanPropertyRowMapper<ManageUser>(ManageUser.class));
			for(ManageUser m:list){
				m.setType(type);
			}
			System.out.println(sql);
			ArrayList<ManageUser> userList = new ArrayList<ManageUser>(list);
			if(userList.size()>0){
				return userList.get(0);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		throw new UsernameNotFoundException("User " + username+ " has no GrantedAuthority");
	}
	
	public String getPwd(String opwd){
		return encoder.encode(opwd);
	}
}