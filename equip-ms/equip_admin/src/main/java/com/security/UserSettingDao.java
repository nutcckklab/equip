package com.security;
 
//import org.springframework.security.core.context.SecurityContextHolder;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import jodd.datetime.JDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.security.crypto.password.StandardPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

 

@Service
public class UserSettingDao {
	
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	@Autowired
	SecurityManager securityManager;
	
	
	//取得關係
	public List<Map<String,Object>> findRelRest(String centerRestId){
		String sql=String.format("SELECT * FROM rest_rel where centerRestId='%s' ORDER BY targetRestId", 
				centerRestId);
		System.out.println(sql);
		return jdbcTemplate.queryForList(sql);
	}
	
	public Object findByType(String type){
		StringBuilder sb=new StringBuilder();
		sb.append("SELECT * FROM sysparam where ");
		
		if(type.length()>0){
			sb.append(" type = '"+type+"' and ");
		}
		if(sb.toString().endsWith("where "))
			sb.delete(sb.length()-6, sb.length()-1);
		if(sb.toString().endsWith("and "))
			sb.delete(sb.length()-4, sb.length()-1);
		sb.append("ORDER BY code ");
		System.out.println(sb.toString());
		return jdbcTemplate.queryForList(sb.toString());
	}
	
	@Transactional
	public void editModel(
			final String userId,
			final String name,
			final String addr,
			final String phone,
			final String email,
			final String pwd
			){
		jdbcTemplate.update("UPDATE userauth SET name=?,phone=?,email=?,addr=?,pwd=? where userId=?",  
                new PreparedStatementSetter() {  
                    public void setValues(PreparedStatement ps) throws SQLException {
                    	int p=1;
                        ps.setString(p++, name);  
                        ps.setString(p++, phone);  
                        ps.setString(p++, email);  
                        ps.setString(p++, addr);  
                        ps.setString(p++, securityManager.getPwd(pwd));  
                        ps.setString(p++, userId);  
                    }  
       });  
	}
	
	@Autowired
	StandardPasswordEncoder encoder;
	
	public Map auth(String userId,String pwd){
//		System.out.println(securityManager.loadUserByUsername(userId).getPassword());
//		System.out.println(securityManager.getPwd(pwd));
//		System.out.println(securityManager.loadUserByUsername(userId).getPassword().equals(securityManager.getPwd(pwd)));
		List<Map<String, Object>> list=jdbcTemplate.queryForList("select * from userauth where userId='"+userId+"'");
		if(list.size()>0){
			if(encoder.matches(pwd,list.get(0).get("pwd").toString())){
				return list.get(0);
			}
			return list.get(0);
		}
		return null;
	}
	
	public Object findById(String userId){
		return jdbcTemplate.queryForList("SELECT * FROM userauth where userId='"+userId+"'").get(0);
	}
	
} 
